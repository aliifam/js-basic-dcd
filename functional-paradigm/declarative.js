const names = ['Harry', 'Ron', 'Jeff', 'Thomas'];

const newNames = names.map((nama) => `${nama}!`)

console.log(newNames)



// what to solve

/*JavaScript sendiri merupakan bahasa pemrograman yang mendukung paradigma FP. 
Banyak Higher-Order Function (kita akan bahas detail tentang ini nanti) yang bisa 
kita manfaatkan sebagai utilitas, salah satunya fungsi array map() di atas.
*/