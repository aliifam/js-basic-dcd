# key

- Mengetahui Paradigma Functional Programming.

- Mengetahui konsep Pure Function, Immutability, dan Higher-Order Function.

- Menggunakan Reusable Function yang ada pada JavaScript.

Functional Programming (selanjutnya akan kita singkat menjadi FP) ditulis dengan gaya deklaratif yang berfokus pada “what to solve” dibanding “how to solve” yang dianut oleh gaya imperatif.