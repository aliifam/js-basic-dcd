const itemTrue = [1, '', 'Hallo', 0, null, 'Harry', 14].filter((item) => Boolean(item))

console.log(itemTrue)

const students = [
    {
        name: 'Harry',
        score: 60,
    },
    {
        name: 'James',
        score: 88,
    },
    {
        name: 'Ron',
        score: 90,
    },
    {
        name: 'Bethy',
        score: 75,
    }
];

const siswaLulus = students.filter((student) => student.score > 85)

console.log(siswaLulus)