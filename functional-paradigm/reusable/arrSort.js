//arr.sort([compareFunction])
//Secara default, fungsi sort akan mengubah semua nilai dalam deretan menjadi bentuk string dan mengurutkannya secara ascending.

/**
 * Pada compare function, fungsi akan membandingkan 2 nilai yang akan menghasilkan 3 result yaitu negatif (-), 0, dan positif (+).

    Jika, negative maka `a` akan diletakkan sebelum `b`
    Jika, positive maka `b` akan diletakkan sebelum `a`
    Jika, 0 maka tidak ada perubahan posisi
 */

const numbers = [1, 30, 4, 1000, 101, 121];

const comparefunc = (a, b) => {
    return a - b;
}

const sortednumbers = numbers.sort(comparefunc);

console.log(sortednumbers)