const hello = () => {
    console.log('Hello')
}

const say = (fungsi) => {
    fungsi();
}

const sayHello = () => {
    return () => {
        console.log('Hello')
    }
}

hello()
say(hello)
sayHello()()

