/**
 * Higher-Order Function merupakan fungsi yang dapat menerima fungsi lainnya pada argumen;
 * mengembalikan sebuah fungsi; atau bahkan keduanya.
 * 
 * Teknik Higher-Order Function biasanya digunakan untuk:

    Mengabstraksi atau mengisolasi sebuah aksi, event, atau menangani alur asynchronous menggunakan callback, promise, dan lainnya.
    Membuat utilities yang dapat digunakan di berbagai tipe data.
    Membuat teknik currying atau function composition.
 */

const names = ['Harry', 'Ron', 'Jeff', 'Thomas'];

const arrayMapManual = (arr, action) => {
    const looptil = (arr, action, newArr = [], index = 0) => {
        const item = arr[index]
        if (!item) return newArr;
        return looptil(arr, action, [...newArr, action(arr[index])], index + 1)
    }
    return looptil(arr, action)
}

const newNames = arrayMapManual(names, (name) => `${name}!`)

console.log({names, newNames})