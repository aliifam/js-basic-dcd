/**
 * Pure Function merupakan konsep dari pembuatan fungsi yang mengharuskan fungsi untuk 
 * tidak bergantung terhadap nilai yang berada di luar fungsi atau parameternya. 
 */

//pure function
const LuasLing = (jarijari) => {
    return 3.14 * jarijari * jarijari
}

console.log(LuasLing(4))

const createPersonWithAge = (person, age) => {
    return {...person, age}
}

const person = {
    name: 'Aliif'
}

const newperson = createPersonWithAge(person, 18)

console.log({
    person,
    newperson
})

