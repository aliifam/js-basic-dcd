//Rekursif merupakan teknik pada sebuah function yang memanggil dirinya sendiri.

const  countdown = count => {
    console.log(count)
    if(count > 0) countdown(count -1)
}

countdown(10)