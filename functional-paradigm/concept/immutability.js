/**
 *  Immutable berarti sebuah objek tidak boleh diubah setelah objek tersebut dibuat
 */

const user = {
    fn: 'Aliif',
    ln: 'Arie'
}

const renameLast = (user, lastname) => {
    return { ...user, ln: lastname }
}

const newUser = renameLast(user, 'Arief')

console.log(user)
console.log(newUser)