class Mail{

    constructor(sender){
        this.from = sender;
    }

    sendEmail(msg, to){
        console.log(`you send: ${msg} to ${to} from ${this.from}`);
    }
}

const mail3 = new Mail('sender@class.com');
mail3.sendEmail('dari class', 'penerima@class.com')

//class seperti no proto