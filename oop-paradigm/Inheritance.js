class Mail {
    constructor(sender){
        this.from = sender
        this._contacts = []
    }

    sendEmail = (msg, to) =>{
        console.log('you send:', msg, 'to', to, 'from', this.from);
        this._contacts.push(to)
    }

    meper(msg){
        console.log(`${msg} Method from Parent`)
    }

    showAllContacts = () => this._contacts;
}

class WhatsApp extends Mail {

    constructor(sender, isBussiness){
        super(sender); //constructor overriding
        this.isBussiness = isBussiness;
    }

    meper(msg){
        super.meper(msg);
        console.log(`${msg} Method from Child`)
    }

    myProfile(){
        return `my name ${this.from}, is ${this.isBussiness ? 'Business' : 'Personal'}`
    }
}

const wa1 = new WhatsApp('aliifam', true)
wa1.sendEmail('hai qeer', 'baqeer');
console.log(wa1.myProfile());
console.log(wa1.showAllContacts())
wa1.meper('test :')
