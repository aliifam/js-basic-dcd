// 1.list abstraksi

const canSendMessage = (self) => ({
    sendMessage: () => console.log('send message:', self.message)
})

const checkValidPhone = (self) => ({
    isValid: () => console.log('valid phone:', self.from)
})

//2. create object composition
const personalEnterprise = (from, message, store) => {
    //3. atribut
    const self = {
        from,
        message,
        store
    }
    //4. method
    const personalEnterpriseBehavior = self => ({
        createCatalog: () => console.log('Catalog created:', self.store)
    })

    //5. create object composition
    return Object.assign(self, personalEnterpriseBehavior(self), canSendMessage(self), checkValidPhone(self))
}

const peng1 = personalEnterprise('sender@gmail.com', 'testing enterprise', 'senderStore');
peng1.createCatalog()
peng1.sendMessage();

