function Mail(){
    this.from = 'senderi@mail.com';
    this.sendEmail = (msg, to) => {
        console.log(`you send: ${msg} to ${to} from ${this.from}`);
    }
}

const mail2 = new Mail()
mail2.sendEmail('hallo', 'penerima@dicoding.com')

console.log(mail2.hasOwnProperty('sendEmail'));