class Mail {
    constructor(sender){
        this.from = sender
        this._contacts = []
    }

    senEmail = (msg, to) =>{
        console.log('you send:', msg, 'to', to, 'from', this.from);
        this._contacts.push(to)
    }

    showAllContacts = () => this._contacts;
}

const mail4 = new Mail('Sender@mail.com')
mail4.senEmail('test1', 'people1@mail.com')
mail4.senEmail('test2', 'people2@mail.com')
mail4.senEmail('test3', 'people3@mail.com')
mail4.senEmail('test4', 'people4@mail.com')

console.log(mail4.showAllContacts())
console.log(mail4._contacts);