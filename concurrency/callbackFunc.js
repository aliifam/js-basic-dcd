const orderkopi = callback => {
    let kopi = null
    console.log("Sedang membuat kopi, silakan tunggu...");
    setTimeout(() => {
        kopi = 'kopi sudah jadi'
        callback(kopi)
    }, 3000);
}

orderkopi(coffee => {
    console.log(coffee)
})