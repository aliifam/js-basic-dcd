# Pengenalan Concurrency

Dalam pemrograman, salah satu topik yang menantang adalah tentang concurrency. Concurrency sendiri berarti beberapa komputasi yang terjadi pada saat yang bersamaan. Sejauh ini kita telah menuliskan kode secara synchronous. Lebih lanjut, pada modul ini kita akan mempelajari beberapa materi seperti:

- Bagaimana menjalankan program secara asynchronous
- Bagaimana menangani kode asynchronous

![](https://dicoding-web-img.sgp1.cdn.digitaloceanspaces.com/original/academy/dos:d0b718e98eb9130ec4fc04572a1bc5b920220613105713.png)
