const state = {
    stock : {
        coffeeBean: 250,
        water: 1000
    },
    isCoffeeMachineBusy: false
}

const checkAvailability = () => {
    return new Promise((res, rej) => {
        setTimeout(() => {
            if (!state.isCoffeeMachineBusy) {
                res('Mesin kopi siap digunakan')
            }else{
                rej('mesin kopi sibuk')
            }
        }, 1000)
    })
}

const checkStock = () => {
    return new Promise((res, rej) => {
        state.isCoffeeMachineBusy = true
        setTimeout(() => {
            if (state.stock.coffeeBean>= 16 && state.stock.water >= 250) {
                res('Stock cukup bisa bikin kopi')
            }else{
                rej('Stock kurang')
            }
        }, 1500);
    })
}

const brewCoffee = () => {
    console.log('membuatkan kopi untuk anda....')
    return new Promise((res, rej) => {
        setTimeout(() => {
            res('kopi sudah siap!')
        }, 2000);
    })
}

const makeEspresso = () => {
    checkAvailability()
    .then((value) => {
        console.log(value)
        return checkStock()
    })
    .then((value) => {
        console.log(value)
        return brewCoffee()
    })
    .then((value) => {
        console.log(value)
        state.isCoffeeMachineBusy = false
    })
    .catch((rejectedReason) => {
        console.log(rejectedReason)
        state.isCoffeeMachineBusy = false;
    })
}

// makeEspresso()

const buatEspresso = async () => {
    try {
        await checkAvailability();
        await checkStock();
        const coffee = await brewCoffee();
        console.log(coffee)
    } catch (rejectedReason) {
        console.log(rejectedReason)
    }
}

buatEspresso();