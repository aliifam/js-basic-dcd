const stock = {
    coffeeBeans: 250,
    water: 1000
}

const checkStock = () =>{
    return new Promise((resolve, reject) =>{ //executor funtion
        if (stock.coffeeBeans >= 16 && stock.water >= 250) {
            resolve('Stock cukup bisa bikin kopi')
        }else{
            reject('Stock kurang')
        }
    })
}

const handleSuccess = (resolvedValue) =>{
    console.log(resolvedValue)
}

const handleFailure = (rejectionReason) =>{
    console.log(rejectionReason)
}

// checkStock().then(handleSuccess, handleFailure); without catch failure

checkStock()
    .then(handleSuccess)
    .catch(handleFailure)