/**
 * sejak ES8 (ECMAScript 2017) kita dapat menuliskan asynchronous process layaknya
 * synchronous process dengan bantuan keyword async dan await.
 */

const getCoffee = () => {
    return new Promise((res, rej) => {
        const seeds = 1;
        setTimeout(() => {
            if (seeds >= 10) {
                res('Kopi di dapatkan')
            }else{
                rej('Biji kopi habis')
            }
        }, 1000);
    })
}

// const makeCoffeee = () =>{
//     getCoffee().then((kopi) => {
//         console.log(kopi)
//     })
// }

// makeCoffeee()  then method

const makeCoffee = async () => {
    try {
        const kopi = await getCoffee()
        console.log(kopi)
    } catch (rejectReason) {
        console.log(rejectReason)
    }
    
}

makeCoffee();