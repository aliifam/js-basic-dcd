const executorFunction = (resolve, reject) => {
    const isCoffeeMachineReady = true
    if (isCoffeeMachineReady) {
        resolve('Kopi berhasil dibuat')
    }else{
        reject('kopi gagal dibuat');
    }
}

const makeCoffee = () => {
    return new Promise(executorFunction)
}

const janjikopi = makeCoffee()
console.log(janjikopi)