const averageExams = (ValuesExam) => {
    const numberValdation = ValuesExam.every((exam) => typeof exam === "number")
    if (!numberValdation) throw Error('plese input with number formar')

    const sumValues = ValuesExam.reduce((acc, currentVal) => acc + currentVal, 0)
    return sumValues / ValuesExam.length
}

const isPassed = (ValuesExam, name) => {
    const minVal = 75
    const average = averageExams(ValuesExam)

    if (average >= minVal) {
        console.log(`${name} passed exam`)
        return true
    }

    console.log(`${name} not passed exam`)
    return false

}

module.exports = { averageExams, isPassed }