const { coffeestock, isCoffeeMachineReady } = require('./state')

const makeCoffee = (type, miligrams) => {
    if (coffeestock[type] >= miligrams) {
        console.log(`kopi ${type} berhasil dibuat`)
    } else {
        console.log(`biji kopi ${type} habis`)
    }
}

makeCoffee('robusta', 80)
console.log(isCoffeeMachineReady)