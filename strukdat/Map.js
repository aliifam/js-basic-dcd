/**
 * Map adalah tipe data yang menyimpan koleksi data dengan format key-value layaknya Object.
 * Yang membedakan adalah Map memperbolehkan key dengan tipe data apa pun, 
 * dibandingkan Object yang hanya mengizinkan key bertipe String atau Symbol.
 * map([key, value], [key, value])
 */

const myMap = new Map([
    ['1', 'String key'],
    [1, 'numberkey'],
    [true, false]
]);

console.log(myMap)

const capital = new Map([
    ["Jakarta", "Indonesia"],
    ["London", "England"],
    ["Tokyo", "Japan"]
]);


console.log(capital.size);
console.log(capital.get("London"))
capital.set("Bangkok", "Thailand")
console.log(capital.size)
console.log(capital.get("Bangkok"))

//WRONG METHOD 
const wrongMap = new Map();

wrongMap["mykey"] = "myval";

console.log(wrongMap)
console.log(wrongMap["mykey"])
console.log(wrongMap.has("My Key"));
console.log(wrongMap.delete("My Key"));