const favorites = ["Seafood", "Salad", "Nugget", "Soup"];
const others = ["Cake", "Pie", "Donut"];

console.log(favorites);

console.log(...favorites);

const allfav = [...favorites, ...others]

console.log(allfav);

const obj1 = { firstName: 'Obi-Wan', age: 30 };
const obj2 = { lastName: 'Kenobi', gender: 'M' };

console.log({...obj1, ...obj2})