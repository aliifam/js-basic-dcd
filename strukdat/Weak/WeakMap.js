/**
 * WeakMap merupakan varian dari Map yang mendukung garbage collection. 
 * 
 * Garbage collection adalah proses di mana interpreter JavaScript mengambil kembali
 * memori yang tidak lagi “dapat dijangkau” dan tidak dapat digunakan oleh program
 * 
 * Garbage collection di JavaScript dilakukan secara otomatis dan bukan menjadi urusan dari developer.
 * 
 * Key dari WeakMap harus berupa object atau array.
 * WeakMap memiliki method get(), set(), has(), dan delete(). Namun, WeakMap tidak termasuk kategori iterable sehingga tidak memiliki method keys(), values(), atau forEach().
 * WeakMap juga tidak memiliki property size. Ini karena ukuran WeakMap dapat berubah karena proses garbage collection.
 */

const { inspect } = require('util')

let visitCountMap = new WeakMap()

function countUser(user){
    let count = visitCountMap.get(user) || 0;
    visitCountMap.set(user, count + 1)
}

let jonas = { name: "jonas"}
countUser(jonas)

jonas = null;

setTimeout(function(){
    console.log(inspect(visitCountMap, { showHidden: true }));
}, 10000);

/**
 * output = WeakMap {  }
 * 
 * Seperti halnya WeakMap, WeakSet adalah versi weak reference dari Set. Perbedaan antara WeakSet dan Set antara lain:

    WeakSet tidak bisa menyimpan nilai primitif.
    WeakSet bukan iterable dan hanya memiliki method add(), has(), dan delete().
    WeakSet tidak memiliki properti size.
 */

