let visitCountMap = new Map();

function countUser(user){
    let count = visitCountMap.get(user) || 0;
    visitCountMap.set(user, count + 1);
}

let jonas = { name: "Jonas"};
countUser(jonas);  // Menambahkan user "Jonas"

jonas = null //jonas dihapus

setTimeout(function(){
    console.log(visitCountMap);
}, 10000);

