const profile = {
    firstName: "John",
    lastName: "Doe",
    age: 18
}

const profile2 = {
    firstName2: "John",
    lastName2: "Doe",
    age2: 18
}

const { firstName, lastName, age } = profile;

console.log(firstName, lastName, age)

const { lastName2 } = profile2;

console.log(lastName2)