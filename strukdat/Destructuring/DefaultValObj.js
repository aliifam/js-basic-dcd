const profile = {
    firstName: "John",
    lastName: "Doe",
    isMale: true,
    age: 18
}

let { firstName, lastName, age, isMale = false } = profile;

console.log(firstName, lastName, age, isMale);