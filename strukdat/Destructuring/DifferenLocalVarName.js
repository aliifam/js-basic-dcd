const profile = {
    firstName: "John",
    lastName: "Doe",
    age: 18
}

const { firstName: panggilan, lastName: marga, age: umur } = profile;

console.log(panggilan, marga, umur)