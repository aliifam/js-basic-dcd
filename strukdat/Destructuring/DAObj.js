const profile = {
    firstName: "John",
    lastName: "Doe",
    age: 18
}
 
let firstName = "Dimas";
let age = 20;

({firstName, lastName, age} = profile)

console.log(firstName, lastName, age)

let a = 1;
let b = 2;

[a, b] = [b, a] //destructuring array untuk swap value

console.log(a)
console.log(b)