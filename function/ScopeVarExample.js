/**
 * Perlu kita perhatikan, jika kita lupa menuliskan keyword let, const, atau var pada script 
 * ketika membuat sebuah variabel, maka variabel tersebut akan menjadi global.
 */

function multiply(num) {
    total = num * num;
    return total;
}

let total = 9;
let number  = multiply(20);

console.log(total)