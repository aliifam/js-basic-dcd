/**
 * jika spread operator menyebarkan array menjadi beberapa elemen berbeda, 
 * rest parameter ini adalah kebalikan dari operator tersebut.
 */

function sum(...ArrAngka){
    let hasil = 0
    for (const angka of ArrAngka) {
        hasil += angka;
    }
    return hasil;
}

console.log(sum(1, 2, 3, 4, 5));