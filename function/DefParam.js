function pangkat(basenumber, exponent = 2){
    let result = basenumber ** exponent
    return `${basenumber} pangkat ${exponent} = ${result}`
}

console.log(pangkat(4))