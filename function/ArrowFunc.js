function menyapa(name){
    return `hai ${name}`; //regular function
}

const sayhi = (name) => {
    return `Halo ${name}`;
}

const kali = (a, b) => a * b;

console.log(menyapa("Aliif"), sayhi("Arief"), kali(10, 20))