const greeting = function(name, lang){
    if (lang === "en") {
        return `Hi ${name} how are U today?`
    } else if (lang === "id") {
        return `Hai ${name} apa kabar hari ini?`
    } else {
        return `parameter kode bahasa "${lang}" tak tersedia`
    }
}

console.log(greeting("Aliif", "en"))