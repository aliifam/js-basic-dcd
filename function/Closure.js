/**
 * Lexical scope berarti pada sebuah fungsi bersarang, 
 * fungsi yang berada di dalam memiliki akses ke variabel di lingkup induknya.
 * 
 * kayak private method kalo di java
 */

 function init() {
    var name = 'Obi Wan';

    function greet() {
        console.log(`Halo, ${name}`);
    }

    return greet;
}

let total = () => {
    let counter = 0;
    return () => {
        return ++counter;
    }
}

let addCounter = total();

console.log(addCounter())
console.log(addCounter())
console.log(addCounter())



