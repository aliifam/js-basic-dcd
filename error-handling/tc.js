/**
 * Object error memiliki beberapa properti utama di dalamnya, yaitu:

    name : Nama error yang terjadi.
    message : Pesan tentang detail error.
    stack : Informasi urutan kejadian yang menyebabkan error. Umumnya digunakan untuk debugging karena terdapat informasi baris mana yang menyebabkan error.
 */


try {
    console.log('awal try')
    errorCode;                      
    console.log("Akhir blok try");
} catch(error){
    console.log(error.name)
    console.log(error.message)
    console.log(error.stack)
} finally {
    console.log("Akan tetap dieksekusi");
}